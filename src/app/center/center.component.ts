import { Component } from '@angular/core';

@Component({
  selector: 'app-center',
  templateUrl: 'center.component.html',
  styleUrls: ['center.component.css']
})
export class CenterComponent {

    showBackgroundHome: boolean = false;
    showBackgroundStories: boolean = false;
    showBackgroundContact: boolean = false;
    showBackgroundAbout: boolean = false;

    openPage(page: string) {

      if(page.endsWith('Contact')){
        if(this.showBackgroundContact==false){
          this.showBackgroundContact = !this.showBackgroundContact;

          this.showBackgroundHome = false; 
          this.showBackgroundStories = false;
          this.showBackgroundAbout = false;
        }
      }

      if(page.endsWith('About')){
        if(this.showBackgroundAbout==false){
          this.showBackgroundAbout = !this.showBackgroundAbout;

          this.showBackgroundHome = false; 
          this.showBackgroundStories = false;
          this.showBackgroundContact = false;
        }
      }

      if(page.endsWith('Stories')){
        if(this.showBackgroundStories==false){
          this.showBackgroundStories = !this.showBackgroundStories;

          this.showBackgroundHome = false; 
          this.showBackgroundContact = false;
          this.showBackgroundAbout = false;
        }
      }

      if(page.endsWith('Home')){
        if(this.showBackgroundHome==false){
          this.showBackgroundHome = !this.showBackgroundHome;

          this.showBackgroundContact = false; 
          this.showBackgroundStories = false;
          this.showBackgroundAbout = false;
        }
      }
    }
}
